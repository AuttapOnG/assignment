package com.example.assignment.network

import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

class SignRequestInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = setHeader(chain)
        return chain.proceed(request.build())
    }

    private fun setHeader(chain: Interceptor.Chain): Request.Builder {
        return chain.request().newBuilder().apply {
            header("api-key", "636e93c080bb67c99b431116f81d361cb6634d60")
        }
    }
}