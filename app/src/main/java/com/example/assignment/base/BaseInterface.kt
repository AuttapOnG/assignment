package com.example.assignment.base

abstract class BaseInterface {
    interface View {
    }

    interface Presenter<V : BaseInterface.View> {
        fun attachView(view: V)
    }
}
