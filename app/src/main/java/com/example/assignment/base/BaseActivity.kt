package com.example.assignment.base

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity

abstract class BaseActivity : AppCompatActivity() {
    protected open fun afterLayout(savedInstanceState: Bundle?) {}
    protected open fun onViewsBound() {}
    @LayoutRes
    protected abstract fun layoutId(): Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutId())
        afterLayout(savedInstanceState)
        onViewsBound()
    }
}