package com.example.assignment.di.module

import com.example.assignment.BuildConfig.BASE_URL
import com.example.domain.ApplicationSchedulerProvider
import com.example.domain.SchedulerProvider
import com.example.assignment.network.SignRequestInterceptor
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

val networkModule = module {
    single { createRetrofitClient() }
    single<SchedulerProvider>(createdAtStart = true) {
        ApplicationSchedulerProvider(
            Schedulers.io(),
            AndroidSchedulers.mainThread()
        )
    }
}

fun createRetrofitClient(): Retrofit {
    val okHttpClient = OkHttpClient.Builder()
        .apply {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY
            addInterceptor(logging)
            addInterceptor(SignRequestInterceptor())
        }.build()
    return Retrofit.Builder()
        .client(okHttpClient)
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()
}