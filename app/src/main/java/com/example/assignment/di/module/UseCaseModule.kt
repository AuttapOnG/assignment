package com.example.assignment.di.module

import com.example.domain.usecase.favorite.FavoriteUseCase
import com.example.domain.usecase.favorite.FavoriteUseCaseImpl
import com.example.domain.usecase.history.HistoryUseCase
import com.example.domain.usecase.history.HistoryUseCaseImpl
import com.example.domain.usecase.search.SearchMovieUseCase
import com.example.domain.usecase.search.SearchMovieUseCaseImpl
import org.koin.dsl.module

val useCaseModule = module {
    single<SearchMovieUseCase> { SearchMovieUseCaseImpl(get(), get(), get()) }
    single<HistoryUseCase> { HistoryUseCaseImpl(get(), get()) }
    single<FavoriteUseCase> { FavoriteUseCaseImpl(get(), get()) }
}