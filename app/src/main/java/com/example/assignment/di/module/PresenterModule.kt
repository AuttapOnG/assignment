package com.example.assignment.di.module

import com.example.assignment.ui.detail.MovieDetailContract
import com.example.assignment.ui.detail.MovieDetailPresenter
import com.example.assignment.ui.favorite.FavoriteContract
import com.example.assignment.ui.favorite.FavoritePresenter
import com.example.assignment.ui.list.MovieListContract
import com.example.assignment.ui.list.MovieListPresenter
import com.example.assignment.ui.main.MainContract
import com.example.assignment.ui.main.MainPresenter
import org.koin.dsl.module

val presenterModule = module {
    single<MainContract.Presenter> { MainPresenter(get(), get()) }
    single<MovieListContract.Presenter> { MovieListPresenter(get()) }
    single<MovieDetailContract.Presenter> { MovieDetailPresenter(get()) }
    single<FavoriteContract.Presenter> { FavoritePresenter(get()) }
}