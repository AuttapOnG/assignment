package com.example.assignment.di.module

import com.example.data.MovieService
import com.example.data.database.AppDatabase
import com.example.data.database.FavoriteDao
import com.example.data.database.SearchHistoryDao
import com.example.data.repository.*
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import retrofit2.Retrofit

val repositoryModule = module {
    single<MovieRepository> {
        MovieRepositoryImpl(
            createService(
                get(),
                MovieService::class.java
            ) as MovieService
        )
    }
    single { AppDatabase.getAppDatabase(androidContext()) }
    single<SearchHistoryRepository> { SearchHistoryRepositoryImpl(getSearchHistoryDao(get())) }
    single<FavoriteRepository> { FavoriteRepositoryImpl(getFavoriteDao(get())) }
}

fun getFavoriteDao(appDatabase: AppDatabase): FavoriteDao {
    return appDatabase.favoriteDao()
}

fun getSearchHistoryDao(appDatabase: AppDatabase): SearchHistoryDao {
    return appDatabase.searchHistoryDao()
}

fun createService(retrofit: Retrofit, cls: Class<*>): Any? {
    return retrofit.create(cls)
}
