package com.example.assignment.ui.main

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.assignment.R
import com.example.assignment.base.BaseFragment
import com.example.assignment.ui.list.MovieListActivity
import com.example.domain.model.movie.MoviePaging
import com.example.domain.model.search.Search
import kotlinx.android.synthetic.main.fragment_main.*
import org.koin.android.ext.android.inject

class MainFragment : BaseFragment(), MainContract.View, SearchAdapter.OnSearchHistoryClickListener {
    private val presenter: MainContract.Presenter by inject()
    private var searchAdapter: SearchAdapter = SearchAdapter(emptyList(), this)

    override fun layoutId(): Int {
        return R.layout.fragment_main
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attachView(this)

        val layoutManager = LinearLayoutManager(activity)
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = searchAdapter

        etSearch.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                searchMovie(etSearch.text.toString())
            }
            true
        }
    }

    override fun onResume() {
        super.onResume()
        presenter.getHistory()
    }

    private fun searchMovie(text: String) {
        presenter.searchMovie(text)
    }

    override fun showResultList(
        result: MoviePaging,
        text: String
    ) {
        val intent = Intent(activity, MovieListActivity::class.java)
        intent.putExtra(MovieListActivity.MovieListActivityDataIntentKey, result)
        intent.putExtra(MovieListActivity.MovieListActivityTitleIntentKey, text)
        activity?.startActivity(intent)
    }

    override fun showHistory(historyList: List<Search>) {
        searchAdapter.setSearchHistoryData(historyList)
    }

    override fun onSearchHistoryClick(text: String) {
        presenter.searchMovie(text)
    }

    override fun showError(throwable: Throwable) {

    }

    companion object {
        fun newInstance(): MainFragment {
            return MainFragment()
        }
    }
}