package com.example.assignment.ui.favorite

import com.example.assignment.base.BaseInterface
import com.example.domain.model.movie.Movie

interface FavoriteContract {
    interface View : BaseInterface.View {
        fun setFavoriteList(favoriteList: List<Movie>)
        fun showError(throwable: Throwable)
    }

    interface Presenter : BaseInterface.Presenter<View> {
        fun getAllFavorite()
    }
}