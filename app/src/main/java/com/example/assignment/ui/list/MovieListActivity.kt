package com.example.assignment.ui.list

import android.os.Bundle
import com.example.assignment.R
import com.example.assignment.base.BaseActivity
import com.example.domain.model.movie.MoviePaging

class MovieListActivity : BaseActivity() {
    companion object {
        const val MovieListActivityDataIntentKey = "MovieListActivityDataIntentKey"
        const val MovieListActivityTitleIntentKey = "MovieListActivityTitleIntentKey"
    }

    override fun layoutId(): Int {
        return R.layout.activity_movie_list
    }

    override fun afterLayout(savedInstanceState: Bundle?) {
        super.afterLayout(savedInstanceState)
        actionBar?.setDisplayHomeAsUpEnabled(true)
        if (savedInstanceState == null) {
            bindFragment()
        }

    }

    private fun bindFragment() {
        val data = intent.getParcelableExtra(MovieListActivityDataIntentKey) as MoviePaging
        val text = intent.getStringExtra(MovieListActivityTitleIntentKey)
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, MovieListFragment.newInstance(data, text))
            .commit()
    }
}