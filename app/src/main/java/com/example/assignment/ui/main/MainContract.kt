package com.example.assignment.ui.main

import com.example.assignment.base.BaseInterface
import com.example.domain.model.movie.MoviePaging
import com.example.domain.model.search.Search

interface MainContract {
    interface View : BaseInterface.View {
        fun showResultList(result: MoviePaging, text: String)
        fun showHistory(historyList: List<Search>)
        fun showError(throwable: Throwable)
    }

    interface Presenter : BaseInterface.Presenter<View>{
        fun getHistory()
        fun searchMovie(text: String)
    }
}
