package com.example.assignment.ui.main

import android.util.Log
import com.example.assignment.base.BasePresenter
import com.example.domain.usecase.history.HistoryUseCase
import com.example.domain.usecase.search.SearchMovieUseCase

class MainPresenter
constructor(
    private val searchMovieUseCase: SearchMovieUseCase,
    private val historyUseCase: HistoryUseCase
) : BasePresenter(),
    MainContract.Presenter {
    private lateinit var view: MainContract.View

    override fun attachView(view: MainContract.View) {
        this.view = view
    }

    override fun searchMovie(text: String) {
        launch {
            searchMovieUseCase.execute(text, 1)
                .subscribe({
                    view.showResultList(it, text)
                }, {
                    view.showError(it)
                })
        }
    }

    override fun getHistory() {
        launch {
            historyUseCase.execute()
                .subscribe({
                    view.showHistory(it)
                }, {
                    view.showError(it)
                })
        }
    }
}