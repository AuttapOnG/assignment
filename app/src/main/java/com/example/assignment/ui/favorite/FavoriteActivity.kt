package com.example.assignment.ui.favorite

import android.os.Bundle
import com.example.assignment.R
import com.example.assignment.base.BaseActivity

class FavoriteActivity : BaseActivity() {
    override fun layoutId(): Int {
        return R.layout.activity_favorite
    }

    override fun afterLayout(savedInstanceState: Bundle?) {
        super.afterLayout(savedInstanceState)
        if (savedInstanceState == null) {
            bindFragment()
        }
    }

    private fun bindFragment() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, FavoriteFragment.newInstance()).commit()
    }
}