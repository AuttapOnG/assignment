package com.example.assignment.ui.list

import com.example.assignment.base.BaseInterface
import com.example.domain.model.movie.Movie
import com.example.domain.model.movie.MoviePaging

interface MovieListContract {
    interface View : BaseInterface.View {
        fun showMovieList(movieList: List<Movie>)
        fun showError(throwable: Throwable)
    }

    interface Presenter : BaseInterface.Presenter<View> {
        fun setData(text: String, data: MoviePaging)
        fun loadMoreData()
        fun canLoadMore(): Boolean
    }
}