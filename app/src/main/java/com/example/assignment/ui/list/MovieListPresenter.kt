package com.example.assignment.ui.list

import com.example.assignment.base.BasePresenter
import com.example.domain.model.movie.MoviePaging
import com.example.domain.usecase.search.SearchMovieUseCase

class MovieListPresenter(
    private val searchMovieUseCase: SearchMovieUseCase
) : BasePresenter(), MovieListContract.Presenter {
    private lateinit var view: MovieListContract.View
    private var text: String? = null
    private var data: MoviePaging? = null
    private var isLoading = false

    override fun attachView(view: MovieListContract.View) {
        this.view = view
    }

    override fun setData(text: String, data: MoviePaging) {
        this.text = text
        this.data = data
        view.showMovieList(data.movieList)
    }

    override fun canLoadMore(): Boolean {
        return data?.let {
            it.page < it.totalPage && !isLoading
        } ?: false
    }

    override fun loadMoreData() {
        text?.let { text ->
            data?.let { data ->
                launch {
                    searchMovieUseCase.execute(text, data.page + 1)
                        .doOnSubscribe {
                            isLoading = true
                        }
                        .doOnSuccess {
                            isLoading = false
                        }
                        .subscribe({
                            setData(text, data)
                        }, {
                            view.showError(it)
                        })
                }
            }
        }
    }
}