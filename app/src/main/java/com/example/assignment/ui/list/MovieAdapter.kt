package com.example.assignment.ui.list

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.domain.model.movie.Movie

class MovieAdapter(private var movieList: List<Movie>, private val listener: MovieClickListener) :
    RecyclerView.Adapter<MovieViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        return MovieViewHolder.createView(parent)
    }

    override fun getItemCount(): Int {
        return movieList.count()
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        holder.bind(movieList[position], listener)
    }

    fun setMoreMovie(movieList: List<Movie>) {
        this.movieList = this.movieList + movieList
        notifyDataSetChanged()
    }

    fun setNewData(movieList: List<Movie>){
        this.movieList = movieList
        notifyDataSetChanged()
    }

    interface MovieClickListener {
        fun onMovieClick(movie: Movie)
    }
}