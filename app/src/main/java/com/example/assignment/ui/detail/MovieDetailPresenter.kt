package com.example.assignment.ui.detail

import com.example.assignment.base.BasePresenter
import com.example.domain.model.movie.Movie
import com.example.domain.usecase.favorite.FavoriteUseCase

class MovieDetailPresenter(
    private val favoriteUseCase: FavoriteUseCase
) : BasePresenter(), MovieDetailContract.Presenter {
    private lateinit var view: MovieDetailContract.View
    private var isFavorite = false

    override fun attachView(view: MovieDetailContract.View) {
        this.view = view
    }

    override fun checkIsFavorite(movie: Movie) {
        launch {
            favoriteUseCase.execute()
                .subscribe({
                    val list = it.filter { favorite ->
                        favorite.id == movie.id
                    }
                    setViewFavorite(list.isNotEmpty())
                }, {
                    view.showError(it)
                })
        }
    }

    override fun clickFavorite(movie: Movie) {
        if (isFavorite) {
            deleteFavorite(movie)
        } else {
            addFavorite(movie)
        }
    }

    private fun addFavorite(movie: Movie) {
        launch {
            favoriteUseCase.addFavorite(movie)
                .subscribe({
                    setViewFavorite(true)
                }, {
                    view.showError(it)
                })
        }
    }

    private fun deleteFavorite(movie: Movie) {
        launch {
            favoriteUseCase.deleteFavorite(movie)
                .subscribe({
                    setViewFavorite(false)
                }, {
                    view.showError(it)
                })
        }
    }

    private fun setViewFavorite(isFavorite: Boolean) {
        this.isFavorite = isFavorite
        view.setButtonFavorite(isFavorite)
    }
}