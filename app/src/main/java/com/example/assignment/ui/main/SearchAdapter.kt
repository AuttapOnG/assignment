package com.example.assignment.ui.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.assignment.R
import com.example.domain.model.movie.Movie
import com.example.domain.model.search.Search

class SearchAdapter(
    private var searchList: List<Search>,
    private val listener: OnSearchHistoryClickListener
) :
    RecyclerView.Adapter<SearchAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.inflateView(parent)
    }

    override fun getItemCount(): Int {
        return searchList.count()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(searchList[position], listener)
    }

    fun setSearchHistoryData(searchList: List<Search>) {
        this.searchList = searchList
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val llRoot = itemView.findViewById<LinearLayout>(R.id.llRoot)
        private val tvTitle = itemView.findViewById<TextView>(R.id.tvTitle)

        companion object {
            fun inflateView(parent: ViewGroup): ViewHolder {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.view_holder_search, parent, false)
                return ViewHolder(view)
            }
        }

        fun bind(search: Search, listener: OnSearchHistoryClickListener) {
            tvTitle.text = search.text
            llRoot.setOnClickListener {
                listener.onSearchHistoryClick(search.text)
            }
        }
    }

    interface OnSearchHistoryClickListener {
        fun onSearchHistoryClick(text: String)
    }
}