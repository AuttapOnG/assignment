package com.example.assignment.ui.favorite

import com.example.assignment.base.BasePresenter
import com.example.domain.usecase.favorite.FavoriteUseCase

class FavoritePresenter(
    private val favoriteUseCase: FavoriteUseCase
) : BasePresenter(), FavoriteContract.Presenter {
    private lateinit var view: FavoriteContract.View

    override fun attachView(view: FavoriteContract.View) {
        this.view = view
    }

    override fun getAllFavorite() {
        launch {
            favoriteUseCase.execute()
                .subscribe({
                    view.setFavoriteList(it)
                }, {
                    view.showError(it)
                })
        }
    }
}