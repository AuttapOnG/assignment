package com.example.assignment.ui.favorite

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.assignment.R
import com.example.assignment.base.BaseFragment
import com.example.assignment.ui.detail.MovieDetailActivity
import com.example.assignment.ui.list.MovieAdapter
import com.example.domain.model.favorite.Favorite
import com.example.domain.model.movie.Movie
import kotlinx.android.synthetic.main.fragment_favorite.*
import org.koin.android.ext.android.inject

class FavoriteFragment : BaseFragment(), FavoriteContract.View, MovieAdapter.MovieClickListener {
    private val presenter: FavoriteContract.Presenter by inject()
    private val adapter = MovieAdapter(emptyList(), this)

    override fun layoutId(): Int {
        return R.layout.fragment_favorite
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attachView(this)

        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.adapter = adapter
    }

    override fun onResume() {
        super.onResume()
        presenter.getAllFavorite()
    }

    override fun setFavoriteList(favoriteList: List<Movie>) {
        adapter.setNewData(favoriteList)
    }

    override fun onMovieClick(movie: Movie) {
        val intent = Intent(activity, MovieDetailActivity::class.java)
        intent.putExtra(MovieDetailActivity.MovieDetailActivityIntentKey, movie)
        activity?.startActivity(intent)
    }

    override fun showError(throwable: Throwable) {

    }

    companion object {
        fun newInstance(): FavoriteFragment {
            return FavoriteFragment()
        }
    }
}