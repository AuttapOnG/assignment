package com.example.assignment.ui.detail

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.example.assignment.R
import com.example.assignment.base.BaseActivity
import com.example.assignment.ui.main.MainActivity
import com.example.domain.model.movie.Movie

class MovieDetailActivity : BaseActivity() {
    companion object {
        const val MovieDetailActivityIntentKey = "MovieDetailActivityIntentKey"
    }

    override fun layoutId(): Int {
        return R.layout.activity_movie_detail
    }

    override fun afterLayout(savedInstanceState: Bundle?) {
        super.afterLayout(savedInstanceState)
        actionBar?.setDisplayHomeAsUpEnabled(true)
        if (savedInstanceState == null) {
            bindFragment()
        }
    }

    private fun bindFragment() {
        val movie = intent.getParcelableExtra<Movie>(MovieDetailActivityIntentKey)
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, MovieDetailFragment.newInstance(movie)).commit()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.detail_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when {
            item.itemId == R.id.backToMain -> {
                val intent = Intent(this, MainActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}