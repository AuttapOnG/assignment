package com.example.assignment.ui.list

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.assignment.R
import com.example.assignment.base.BaseFragment
import com.example.assignment.ui.detail.MovieDetailActivity
import com.example.domain.model.movie.Movie
import com.example.domain.model.movie.MoviePaging
import kotlinx.android.synthetic.main.fragment_movie_list.*
import org.koin.android.ext.android.inject

class MovieListFragment(private val text: String, private val data: MoviePaging) : BaseFragment(),
    MovieListContract.View, MovieAdapter.MovieClickListener {

    private val presenter: MovieListContract.Presenter by inject()
    private val adapter = MovieAdapter(emptyList(), this)

    override fun layoutId(): Int {
        return R.layout.fragment_movie_list
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attachView(this)

        val layoutManager = LinearLayoutManager(activity)
        rvMovie.layoutManager = layoutManager
        rvMovie.adapter = adapter

        rvMovie.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                val lastPosition = layoutManager.findLastVisibleItemPosition()
                if (lastPosition == adapter.itemCount - 1 && presenter.canLoadMore()) {
                    presenter.loadMoreData()
                }
            }
        })

        presenter.setData(text, data)
    }

    override fun showMovieList(movieList: List<Movie>) {
        adapter.setMoreMovie(movieList)
    }

    override fun showError(message: Throwable) {

    }

    override fun onMovieClick(movie: Movie) {
        val intent = Intent(activity, MovieDetailActivity::class.java)
        intent.putExtra(MovieDetailActivity.MovieDetailActivityIntentKey, movie)
        activity?.startActivity(intent)
    }

    companion object {
        fun newInstance(data: MoviePaging, text: String): MovieListFragment {
            return MovieListFragment(text, data)
        }
    }
}