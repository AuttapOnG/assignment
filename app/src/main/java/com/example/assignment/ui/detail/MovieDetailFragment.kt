package com.example.assignment.ui.detail

import android.os.Bundle
import android.view.View
import com.bumptech.glide.Glide
import com.example.assignment.BuildConfig
import com.example.assignment.R
import com.example.assignment.base.BaseFragment
import com.example.domain.model.movie.Movie
import kotlinx.android.synthetic.main.fragment_movie_detail.*
import org.koin.android.ext.android.inject

class MovieDetailFragment(private val movie: Movie) : BaseFragment(), MovieDetailContract.View {
    private val presenter: MovieDetailContract.Presenter by inject()

    override fun layoutId(): Int {
        return R.layout.fragment_movie_detail
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attachView(this)
        presenter.checkIsFavorite(movie)

        Glide.with(ivPoster).load(BuildConfig.BASE_POSTER_URL + movie.backdropPath).into(ivPoster)
        tvMovieDetailTitle.text = movie.title
        tvVoteAverage.text = "Average votes : ${movie.voteAverage}"
        tvOverview.text = movie.overview

        bAddFavorite.setOnClickListener {
            presenter.clickFavorite(movie)
        }
    }

    override fun setButtonFavorite(isFavorite: Boolean) {
        if (isFavorite) {
            bAddFavorite.text = "Unfavorite"
        } else {
            bAddFavorite.text = "Favorite"
        }
    }

    override fun showError(throwable: Throwable) {

    }

    companion object {
        fun newInstance(movie: Movie): MovieDetailFragment {
            return MovieDetailFragment(movie)
        }
    }
}