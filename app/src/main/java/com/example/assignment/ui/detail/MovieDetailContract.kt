package com.example.assignment.ui.detail

import com.example.assignment.base.BaseInterface
import com.example.domain.model.movie.Movie

interface MovieDetailContract {
    interface View : BaseInterface.View {
        fun setButtonFavorite(isFavorite: Boolean)
        fun showError(throwable: Throwable)
    }

    interface Presenter : BaseInterface.Presenter<View> {
        fun checkIsFavorite(movie: Movie)
        fun clickFavorite(movie: Movie)
    }
}