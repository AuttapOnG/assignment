package com.example.assignment.ui.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.assignment.BuildConfig.BASE_POSTER_URL
import com.example.assignment.R
import com.example.domain.model.movie.Movie

class MovieViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    private val llRoot = itemView.findViewById<LinearLayout>(R.id.llRoot)
    private val ivPoster = itemView.findViewById<ImageView>(R.id.ivPoster)
    private val tvTitle = itemView.findViewById<TextView>(R.id.tvTitle)
    private val tvReleaseDate = itemView.findViewById<TextView>(R.id.tvReleaseDate)
    private val tvOverview = itemView.findViewById<TextView>(R.id.tvOverview)

    companion object {
        fun createView(parent: ViewGroup): MovieViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.view_holder_movie, parent, false)
            return MovieViewHolder(view)
        }
    }

    fun bind(
        movie: Movie,
        listener: MovieAdapter.MovieClickListener
    ) {
        Glide.with(ivPoster).load(BASE_POSTER_URL + movie.backdropPath).into(ivPoster)
        tvTitle.text = movie.title
        tvReleaseDate.text = movie.releaseDate
        tvOverview.text = movie.overview

        llRoot.setOnClickListener {
            listener.onMovieClick(movie)
        }
    }
}