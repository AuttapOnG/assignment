package com.example.assignment

import android.app.Application
import com.example.assignment.di.module.networkModule
import com.example.assignment.di.module.presenterModule
import com.example.assignment.di.module.repositoryModule
import com.example.assignment.di.module.useCaseModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class AssignmentApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@AssignmentApplication)
            modules(
                networkModule,
                repositoryModule,
                useCaseModule,
                presenterModule
            )
        }
    }
}