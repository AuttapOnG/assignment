package com.example.assignment

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import com.example.assignment.ui.list.MovieViewHolder
import com.example.assignment.ui.main.MainActivity
import org.hamcrest.core.SubstringMatcher
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class SearchTestSuite {

    @get:Rule
    var mainActivityActivityTestRule: ActivityTestRule<MainActivity> =
        ActivityTestRule(MainActivity::class.java)

    var etSearch: ViewInteraction = onView(withId(R.id.etSearch))
    var rvMovie: ViewInteraction = onView(withId(R.id.rvMovie))

    @Test
    fun searchText() {
        val text = "bat"
        etSearch.perform(typeText(text))
        etSearch.perform(pressImeActionButton())

        waitForElementToAppear(rvMovie)
        rvMovie.check(matches(hasMinimumChildCount(1)))
        onView(RecyclerViewMatcher(R.id.rvMovie).atPositionOnView(0, R.id.tvTitle)).check(
            matches(withText(ContainsIgnoreCase(text)))
        )
    }

    @Test
    fun showDetail() {
        searchText()
        rvMovie.perform(RecyclerViewActions.scrollToPosition<MovieViewHolder>(15))
        rvMovie.perform(RecyclerViewActions.actionOnItemAtPosition<MovieViewHolder>(15, click()))
        waitForElementToAppear(onView(withId(R.id.ivPoster)))
    }

    @Test
    fun clickFavorite() {
        showDetail()
        waitForElementToAppear(onView(withId(R.id.bAddFavorite)))
        onView(withId(R.id.bAddFavorite)).check(matches(isDisplayed()))

        onView(withId(R.id.scrollView)).perform(swipeUp())
        onView(withId(R.id.bAddFavorite)).perform(click())
    }

    @Test
    fun clickBackToMain() {
        showDetail()

        waitForElementToAppear(onView(withId(R.id.backToMain)))
        onView(withId(R.id.backToMain)).perform(click())
        waitForElementToAppear(etSearch)
    }

    @Test
    fun clickRecent() {
        waitForElementToAppear(etSearch)
        onView(withId(R.id.recyclerView)).perform(
            RecyclerViewActions.actionOnItemAtPosition<MovieViewHolder>(
                0,
                click()
            )
        )
        waitForElementToAppear(rvMovie)
        rvMovie.perform(RecyclerViewActions.scrollToPosition<MovieViewHolder>(15))
        rvMovie.perform(RecyclerViewActions.actionOnItemAtPosition<MovieViewHolder>(15, click()))
        waitForElementToAppear(onView(withId(R.id.ivPoster)))
    }

    @Test
    fun clickShowFavorite() {
        waitForElementToAppear(onView(withId(R.id.navToFavorite)))
        onView(withId(R.id.navToFavorite)).perform(click())
        waitForElementToAppear(onView(withId(R.id.recyclerView)))
        onView(withId(R.id.recyclerView)).perform(
            RecyclerViewActions.actionOnItemAtPosition<MovieViewHolder>(
                0,
                click()
            )
        )
        waitForElementToAppear(onView(withId(R.id.ivPoster)))
    }

    class ContainsIgnoreCase(substring: String) : SubstringMatcher(substring) {

        override fun relationship(): String {
            return "ContainsIgnoreCase $substring"
        }

        override fun evalSubstringOf(string: String?): Boolean {
            string?.let {
                return it.indexOf(substring, 0, true) >= 0
            } ?: return false
        }
    }
}