package com.example.assignment

import android.content.res.Resources
import android.util.Log
import android.view.View
import androidx.test.espresso.UiController
import androidx.test.espresso.matcher.ViewMatchers.isRoot
import androidx.test.espresso.ViewAction
import org.hamcrest.Matcher
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import org.hamcrest.Description
import org.hamcrest.TypeSafeMatcher
import org.hamcrest.core.SubstringMatcher

fun waitFor(millis: Long): ViewAction {
    return object : ViewAction {
        override fun getConstraints(): Matcher<View> {
            return isRoot()
        }

        override fun getDescription(): String {
            return "Wait for $millis milliseconds."
        }

        override fun perform(uiController: UiController, view: View) {
            uiController.loopMainThreadForAtLeast(millis)
        }
    }
}

class RecyclerViewMatcher(private val recyclerViewId: Int) {

    fun atPosition(position: Int): Matcher<View> {
        return atPositionOnView(position, -1)
    }

    fun atPositionOnView(position: Int, targetViewId: Int): Matcher<View> {

        return object : TypeSafeMatcher<View>() {
            internal var resources: Resources? = null
            internal var childView: View? = null

            override fun describeTo(description: Description) {
                var idDescription = Integer.toString(recyclerViewId)
                if (this.resources != null) {
                    try {
                        idDescription = this.resources!!.getResourceName(recyclerViewId)
                    } catch (var4: Resources.NotFoundException) {
                        idDescription = String.format(
                            "%s (resource name not found)",
                            *arrayOf<Any>(Integer.valueOf(recyclerViewId))
                        )
                    }

                }

                description.appendText("with id: $idDescription")
            }

            override fun matchesSafely(view: View): Boolean {

                this.resources = view.resources

                if (childView == null) {
                    val recyclerView =
                        view.rootView.findViewById<View>(recyclerViewId) as RecyclerView
                    if (recyclerView != null && recyclerView.id == recyclerViewId) {
                        childView =
                            recyclerView.findViewHolderForAdapterPosition(position)!!.itemView
                    } else {
                        return false
                    }
                }

                if (targetViewId == -1) {
                    return view === childView
                } else {
                    val targetView = childView!!.findViewById<View>(targetViewId)
                    return view === targetView
                }

            }
        }
    }
}

private val TIMEOUT_MILLISECONDS = 5000
private val SLEEP_MILLISECONDS = 1000

fun waitForElementToAppear(interaction: ViewInteraction, millis: Long = TIMEOUT_MILLISECONDS.toLong()): Boolean? {
    val maxAttempts = millis / SLEEP_MILLISECONDS
    var i = 0
    while (i++ < (1 + maxAttempts.toInt())) {
        try {
            interaction.check(matches(isDisplayed()))
            Log.i("ViewChecker", "Not sleeping")
            Thread.sleep(200)
            return true
        } catch (e: Throwable) {
            e.printStackTrace()
            Log.i("ViewChecker", "sleeping")
            try {
                Thread.sleep(SLEEP_MILLISECONDS.toLong())
            } catch (e: Exception) {
                Log.i("Sleep", "sleeping error")
            }
        }
    }
    return false
}