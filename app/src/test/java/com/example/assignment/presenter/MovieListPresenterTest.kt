package com.example.assignment.presenter

import com.example.assignment.ui.list.MovieListContract
import com.example.assignment.ui.list.MovieListPresenter
import com.example.domain.model.movie.MoviePaging
import com.example.domain.usecase.search.SearchMovieUseCase
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.internal.util.reflection.FieldSetter
import org.powermock.core.classloader.annotations.PrepareForTest
import org.powermock.modules.junit4.PowerMockRunner

@RunWith(PowerMockRunner::class)
@PrepareForTest(MoviePaging::class)
class MovieListPresenterTest {
    lateinit var presenter: MovieListContract.Presenter

    @Mock
    lateinit var searchMovieUseCase: SearchMovieUseCase

    @Mock
    lateinit var view: MovieListContract.View

    @Mock
    lateinit var moviePaging: MoviePaging

    private val pageTitle = "title"
    private val page = 1
    private val totalPage = 3

    @Before
    fun setUp() {
        presenter = MovieListPresenter(searchMovieUseCase)
        presenter.attachView(view)
    }

    @Test
    fun setDataTest() {
        presenter.setData(pageTitle, moviePaging)
        verify(view).showMovieList(moviePaging.movieList)
    }

    @Test
    fun canLoadMoreTest() {
        val dataMovie = MoviePaging(page, totalPage, emptyList())
        FieldSetter.setField(presenter, presenter.javaClass.getDeclaredField("data"), dataMovie)
        FieldSetter.setField(presenter, presenter.javaClass.getDeclaredField("isLoading"), false)
        val data = presenter.canLoadMore()
        assertTrue(data)
    }

    @Test
    fun canLoadMoreTestDataNull() {
        val data = presenter.canLoadMore()
        assertFalse(data)
    }

    @Test
    fun loadMoreDataTestSuccess() {
        val dataMovie = MoviePaging(page, totalPage, emptyList())
        FieldSetter.setField(presenter, presenter.javaClass.getDeclaredField("data"), dataMovie)
        FieldSetter.setField(presenter, presenter.javaClass.getDeclaredField("text"), pageTitle)

        whenever(
            searchMovieUseCase.execute(
                pageTitle,
                dataMovie.page + 1
            )
        ).thenReturn(Single.just(moviePaging))

        presenter.loadMoreData()

        verify(searchMovieUseCase).execute(pageTitle, dataMovie.page + 1)
        verify(view).showMovieList(moviePaging.movieList)
    }

    @Test
    fun loadMoreDataTestError() {
        val dataMovie = MoviePaging(page, totalPage, emptyList())
        FieldSetter.setField(presenter, presenter.javaClass.getDeclaredField("data"), dataMovie)
        FieldSetter.setField(presenter, presenter.javaClass.getDeclaredField("text"), pageTitle)

        val errorMessage = "message"
        val throwable = Throwable(errorMessage)
        val response = Single.error<MoviePaging>(throwable)
        whenever(searchMovieUseCase.execute(pageTitle, dataMovie.page + 1)).thenReturn(response)

        presenter.loadMoreData()

        verify(searchMovieUseCase).execute(pageTitle, dataMovie.page + 1)
        verify(view).showError(throwable)
    }
}