package com.example.assignment.presenter

import com.example.assignment.ui.favorite.FavoriteContract
import com.example.assignment.ui.favorite.FavoritePresenter
import com.example.domain.model.movie.Movie
import com.example.domain.usecase.favorite.FavoriteUseCase
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.powermock.modules.junit4.PowerMockRunner

@RunWith(PowerMockRunner::class)
class FavoritePresenterTest {
    lateinit var presenter: FavoriteContract.Presenter

    @Mock
    lateinit var favoriteUseCase: FavoriteUseCase

    @Mock
    lateinit var view: FavoriteContract.View

    @Before
    fun setUp() {
        presenter = FavoritePresenter(favoriteUseCase)
        presenter.attachView(view)
    }

    @Test
    fun getAllFavoriteTestSuccess() {
        val list = emptyList<Movie>()
        whenever(favoriteUseCase.execute()).thenReturn(Single.just(list))

        presenter.getAllFavorite()

        verify(favoriteUseCase).execute()
        verify(view).setFavoriteList(list)
    }

    @Test
    fun getAllFavoriteTestError() {
        val errorMessage = "message"
        val throwable = Throwable(errorMessage)
        whenever(favoriteUseCase.execute()).thenReturn(Single.error(throwable))

        presenter.getAllFavorite()

        verify(favoriteUseCase).execute()
        verify(view).showError(throwable)
    }
}