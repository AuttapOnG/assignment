package com.example.assignment.presenter

import com.example.assignment.ui.main.MainContract
import com.example.assignment.ui.main.MainPresenter
import com.example.domain.model.movie.MoviePaging
import com.example.domain.model.search.Search
import com.example.domain.usecase.history.HistoryUseCase
import com.example.domain.usecase.search.SearchMovieUseCase
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.powermock.core.classloader.annotations.PrepareForTest
import org.powermock.modules.junit4.PowerMockRunner

@RunWith(PowerMockRunner::class)
@PrepareForTest(MoviePaging::class)
class MainPresenterTest {
    lateinit var presenter: MainContract.Presenter

    @Mock
    lateinit var searchMovieUseCase: SearchMovieUseCase

    @Mock
    lateinit var historyUseCase: HistoryUseCase

    @Mock
    lateinit var view: MainContract.View

    @Mock
    lateinit var moviePaging: MoviePaging

    private val pageTitle = "title"
    private val page = 1
    private val totalPage = 3
    private val errorMessage = "message"
    private val throwable = Throwable(errorMessage)

    @Before
    fun setUp() {
        presenter = MainPresenter(searchMovieUseCase, historyUseCase)
        presenter.attachView(view)
    }

    @Test
    fun searchMovieTestSuccess() {
        whenever(searchMovieUseCase.execute(pageTitle, page)).thenReturn(Single.just(moviePaging))

        presenter.searchMovie(pageTitle)

        verify(searchMovieUseCase).execute(pageTitle, page)
        verify(view).showResultList(moviePaging, pageTitle)
    }

    @Test
    fun searchMovieTestError() {
        whenever(searchMovieUseCase.execute(pageTitle, page)).thenReturn(Single.error(throwable))

        presenter.searchMovie(pageTitle)

        verify(searchMovieUseCase).execute(pageTitle, page)
        verify(view).showError(throwable)
    }

    @Test
    fun getHistoryTestSuccess() {
        val list = emptyList<Search>()
        whenever(historyUseCase.execute()).thenReturn(Single.just(list))

        presenter.getHistory()

        verify(historyUseCase).execute()
        verify(view).showHistory(list)
    }

    @Test
    fun getHistoryTestError() {
        whenever(historyUseCase.execute()).thenReturn(Single.error(throwable))

        presenter.getHistory()

        verify(historyUseCase).execute()
        verify(view).showError(throwable)
    }
}