package com.example.assignment.presenter

import com.example.assignment.ui.detail.MovieDetailContract
import com.example.assignment.ui.detail.MovieDetailPresenter
import com.example.assignment.ui.main.MainPresenter
import com.example.domain.model.movie.Movie
import com.example.domain.usecase.favorite.FavoriteUseCase
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.internal.util.reflection.FieldSetter
import org.powermock.core.classloader.annotations.PrepareForTest
import org.powermock.modules.junit4.PowerMockRunner

@RunWith(PowerMockRunner::class)
@PrepareForTest(Movie::class)
class MovieDetailPresenterTest {
    lateinit var presenter: MovieDetailContract.Presenter

    @Mock
    lateinit var favoriteUseCase: FavoriteUseCase

    @Mock
    lateinit var view: MovieDetailContract.View

    @Mock
    lateinit var movie: Movie

    private val errorMessage = "message"
    private val throwable = Throwable(errorMessage)

    @Before
    fun setUp() {
        presenter = MovieDetailPresenter(favoriteUseCase)
        presenter.attachView(view)
    }

    @Test
    fun checkFavoriteTestSuccess() {
        val list = emptyList<Movie>()
        whenever(favoriteUseCase.execute()).thenReturn(Single.just(list))

        presenter.checkIsFavorite(movie)

        verify(favoriteUseCase).execute()
        verify(view).setButtonFavorite(list.isNotEmpty())
    }

    @Test
    fun checkUnfavoriteTestSuccess() {
        val list = listOf(movie)
        whenever(favoriteUseCase.execute()).thenReturn(Single.just(list))

        presenter.checkIsFavorite(movie)

        verify(favoriteUseCase).execute()
        verify(view).setButtonFavorite(list.isNotEmpty())
    }

    @Test
    fun checkIsFavoriteTestError() {
        whenever(favoriteUseCase.execute()).thenReturn(Single.error(throwable))

        presenter.checkIsFavorite(movie)

        verify(favoriteUseCase).execute()
        verify(view).showError(throwable)
    }

    @Test
    fun addFavoriteTestSuccess() {
        FieldSetter.setField(presenter, presenter.javaClass.getDeclaredField("isFavorite"), false)
        whenever(favoriteUseCase.addFavorite(movie)).thenReturn(Single.just(Unit))

        presenter.clickFavorite(movie)

        verify(favoriteUseCase).addFavorite(movie)
        verify(view).setButtonFavorite(true)
    }

    @Test
    fun addFavoriteTestError() {
        FieldSetter.setField(presenter, presenter.javaClass.getDeclaredField("isFavorite"), false)
        whenever(favoriteUseCase.addFavorite(movie)).thenReturn(Single.error(throwable))

        presenter.clickFavorite(movie)

        verify(favoriteUseCase).addFavorite(movie)
        verify(view).showError(throwable)
    }

    @Test
    fun deleteFavoriteTextSuccess() {
        FieldSetter.setField(presenter, presenter.javaClass.getDeclaredField("isFavorite"), true)
        whenever(favoriteUseCase.deleteFavorite(movie)).thenReturn(Single.just(Unit))

        presenter.clickFavorite(movie)

        verify(favoriteUseCase).deleteFavorite(movie)
        verify(view).setButtonFavorite(false)
    }

    @Test
    fun deleteFavoriteTextError() {
        FieldSetter.setField(presenter, presenter.javaClass.getDeclaredField("isFavorite"), true)
        whenever(favoriteUseCase.deleteFavorite(movie)).thenReturn(Single.error(throwable))

        presenter.clickFavorite(movie)

        verify(favoriteUseCase).deleteFavorite(movie)
        verify(view).showError(throwable)
    }
}