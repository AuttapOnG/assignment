package com.example.domain.usecase.search

import com.example.domain.model.movie.MoviePaging
import io.reactivex.Single

interface SearchMovieUseCase {
    fun execute(title: String, paging: Int): Single<MoviePaging>
}