package com.example.domain.usecase.favorite

import com.example.domain.model.movie.Movie
import io.reactivex.Single

interface FavoriteUseCase {
    fun execute(): Single<List<Movie>>
    fun addFavorite(movie: Movie): Single<Unit>
    fun deleteFavorite(movie: Movie): Single<Unit>
}