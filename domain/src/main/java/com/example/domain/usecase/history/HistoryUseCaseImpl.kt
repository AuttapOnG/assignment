package com.example.domain.usecase.history

import com.example.data.repository.SearchHistoryRepository
import com.example.domain.SchedulerProvider
import com.example.domain.model.search.Search
import io.reactivex.Single

class HistoryUseCaseImpl constructor(
    private val schedulerProvider: SchedulerProvider,
    private val searchHistoryRepository: SearchHistoryRepository
) : HistoryUseCase {

    private fun buildUseCaseSingle(): Single<List<Search>> =
        searchHistoryRepository.getAll()
            .map {
                it.map {
                    Search(it.text)
                }
            }

    override fun execute(): Single<List<Search>> {
        return buildUseCaseSingle()
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
    }
}