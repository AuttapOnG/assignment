package com.example.domain.usecase.history

import com.example.domain.model.search.Search
import io.reactivex.Single

interface HistoryUseCase {
    fun execute(): Single<List<Search>>
}