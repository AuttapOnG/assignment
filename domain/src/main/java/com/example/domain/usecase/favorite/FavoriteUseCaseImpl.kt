package com.example.domain.usecase.favorite

import com.example.data.database.FavoriteEntity
import com.example.data.repository.FavoriteRepository
import com.example.domain.SchedulerProvider
import com.example.domain.model.favorite.Favorite
import com.example.domain.model.movie.Movie
import io.reactivex.Flowable
import io.reactivex.Single

class FavoriteUseCaseImpl constructor(
    private val schedulerProvider: SchedulerProvider,
    private val favoriteRepository: FavoriteRepository
) : FavoriteUseCase {
    private fun buildUseCaseSingle(): Single<List<Movie>> =
        favoriteRepository.getAllFavorite()
            .map {
                it.map {
                    Movie(
                        it.id,
                        it.title,
                        it.overview,
                        it.backdropPath,
                        it.releaseDate,
                        it.voteAverage
                    )
                }
            }

    override fun execute(): Single<List<Movie>> {
        return buildUseCaseSingle()
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
    }

    override fun addFavorite(movie: Movie): Single<Unit> {
        return Single.fromCallable {
            favoriteRepository.addFavorite(
                FavoriteEntity(
                    movie.id,
                    movie.title,
                    movie.overview,
                    movie.backdropPath,
                    movie.releaseDate,
                    movie.voteAverage
                )
            )
        }
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())

    }

    override fun deleteFavorite(movie: Movie): Single<Unit> {
        return Single.fromCallable {
            favoriteRepository.deleteFavorite(
                FavoriteEntity(
                    movie.id,
                    movie.title,
                    movie.overview,
                    movie.backdropPath,
                    movie.releaseDate,
                    movie.voteAverage
                )
            )
        }
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
    }
}