package com.example.domain.usecase.search

import com.example.data.database.SearchHistoryEntity
import com.example.data.model.search.SearchMovieRequest
import com.example.data.repository.MovieRepository
import com.example.data.repository.SearchHistoryRepository
import com.example.domain.SchedulerProvider
import com.example.domain.model.movie.Movie
import com.example.domain.model.movie.MoviePaging
import io.reactivex.Single

class SearchMovieUseCaseImpl(
    private val schedulerProvider: SchedulerProvider,
    private val movieRepository: MovieRepository,
    private val searchHistoryRepository: SearchHistoryRepository
) : SearchMovieUseCase {
    private fun buildUseCaseSingle(title: String, paging: Int): Single<MoviePaging> {
        return movieRepository.searchMovie(SearchMovieRequest(title, paging))
            .map {
                MoviePaging(it.page,
                    it.totalPages,
                    it.results.map {
                        Movie(
                            it.id,
                            it.title,
                            it.overview,
                            it.backdropPath,
                            it.releaseDate,
                            it.voteAverage
                        )
                    })
            }.doAfterSuccess {
                searchHistoryRepository.insert(SearchHistoryEntity(title))
            }
    }

    override fun execute(title: String, paging: Int): Single<MoviePaging> {
        return buildUseCaseSingle(title, paging)
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
    }
}