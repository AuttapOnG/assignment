package com.example.domain

import io.reactivex.Scheduler

class ApplicationSchedulerProvider constructor(
    private val io: Scheduler,
    private val ui: Scheduler
) : SchedulerProvider {
    override fun io() = io
    override fun ui() = ui
}