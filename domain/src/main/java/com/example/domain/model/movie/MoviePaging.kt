package com.example.domain.model.movie

import android.annotation.SuppressLint
import android.os.Parcel
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

data class MoviePaging(var page: Int, var totalPage: Int, var movieList: List<Movie>) : Parcelable {
    constructor(source: Parcel) : this(
        source.readInt(),
        source.readInt(),
        ArrayList<Movie>().apply { source.readList(this as List<*>, Movie::class.java.classLoader) }
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeInt(page)
        writeInt(totalPage)
        writeList(movieList)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<MoviePaging> = object : Parcelable.Creator<MoviePaging> {
            override fun createFromParcel(source: Parcel): MoviePaging = MoviePaging(source)
            override fun newArray(size: Int): Array<MoviePaging?> = arrayOfNulls(size)
        }
    }
}