package com.example.domain.model.search

data class Search(val text: String)