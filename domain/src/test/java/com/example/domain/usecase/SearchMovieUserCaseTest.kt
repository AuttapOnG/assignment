package com.example.domain.usecase

import com.example.data.model.search.SearchMovieRequest
import com.example.data.model.search.SearchResponse
import com.example.data.repository.MovieRepository
import com.example.data.repository.SearchHistoryRepository
import com.example.domain.ApplicationSchedulerProvider
import com.example.domain.model.movie.MoviePaging
import com.example.domain.usecase.search.SearchMovieUseCase
import com.example.domain.usecase.search.SearchMovieUseCaseImpl
import com.nhaarman.mockitokotlin2.*
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.powermock.modules.junit4.PowerMockRunner

@RunWith(PowerMockRunner::class)
class SearchMovieUserCaseTest {
    lateinit var searchMovieUseCase: SearchMovieUseCase

    @Mock
    lateinit var movieRepository: MovieRepository

    @Mock
    lateinit var searchHistoryRepository: SearchHistoryRepository

    private val searchResponse = SearchResponse(1, 1, emptyList())

    lateinit var schedulerProvider: ApplicationSchedulerProvider

    @Before
    fun setUp() {
        schedulerProvider =
            ApplicationSchedulerProvider(
                Schedulers.trampoline(),
                Schedulers.trampoline()
            )

        searchMovieUseCase = SearchMovieUseCaseImpl(
            schedulerProvider,
            movieRepository,
            searchHistoryRepository
        )
    }

    @Test
    fun executeTest() {
        val searchMovieRequest = SearchMovieRequest("title", 1)
        whenever(movieRepository.searchMovie(searchMovieRequest)).thenReturn(
            Single.just(
                searchResponse
            )
        )

        val observer = TestObserver<MoviePaging>()
        searchMovieUseCase.execute("title", 1).subscribe(observer)

        verify(movieRepository).searchMovie(searchMovieRequest)
        observer.assertNoErrors()
        observer.assertComplete()
    }
}