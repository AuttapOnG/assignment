package com.example.domain.usecase

import com.example.data.database.SearchHistoryEntity
import com.example.data.repository.SearchHistoryRepository
import com.example.domain.ApplicationSchedulerProvider
import com.example.domain.model.search.Search
import com.example.domain.usecase.history.HistoryUseCase
import com.example.domain.usecase.history.HistoryUseCaseImpl
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.powermock.modules.junit4.PowerMockRunner

@RunWith(PowerMockRunner::class)
class HistoryUseCaseTest {
    lateinit var historyUseCase: HistoryUseCase

    @Mock
    lateinit var searchHistoryRepository: SearchHistoryRepository

    lateinit var schedulerProvider: ApplicationSchedulerProvider

    @Before
    fun setUp() {
        schedulerProvider =
            ApplicationSchedulerProvider(
                Schedulers.trampoline(),
                Schedulers.trampoline()
            )
        historyUseCase = HistoryUseCaseImpl(schedulerProvider, searchHistoryRepository)
    }

    @Test
    fun execute() {
        val list = emptyList<SearchHistoryEntity>()
        whenever(searchHistoryRepository.getAll()).thenReturn(Single.just(list))

        val observer = TestObserver<List<Search>>()
        historyUseCase.execute().subscribe(observer)

        verify(searchHistoryRepository).getAll()
        observer.assertNoErrors()
        observer.assertComplete()
    }
}