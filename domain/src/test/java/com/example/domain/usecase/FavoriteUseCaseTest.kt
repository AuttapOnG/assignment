package com.example.domain.usecase

import com.example.data.database.FavoriteEntity
import com.example.data.repository.FavoriteRepository
import com.example.domain.ApplicationSchedulerProvider
import com.example.domain.model.movie.Movie
import com.example.domain.usecase.favorite.FavoriteUseCase
import com.example.domain.usecase.favorite.FavoriteUseCaseImpl
import com.nhaarman.mockitokotlin2.*
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.powermock.core.classloader.annotations.PrepareForTest
import org.powermock.modules.junit4.PowerMockRunner

@RunWith(PowerMockRunner::class)
@PrepareForTest(FavoriteEntity::class, Movie::class)
class FavoriteUseCaseTest {
    lateinit var favoriteUseCase: FavoriteUseCase

    @Mock
    lateinit var favoriteRepository: FavoriteRepository

    lateinit var schedulerProvider: ApplicationSchedulerProvider

    @Mock
    lateinit var favoriteEntity: FavoriteEntity

    @Mock
    lateinit var movie: Movie

    @Before
    fun setUp() {
        schedulerProvider =
            ApplicationSchedulerProvider(
                Schedulers.trampoline(),
                Schedulers.trampoline()
            )
        favoriteUseCase = FavoriteUseCaseImpl(schedulerProvider, favoriteRepository)
    }

    @Test
    fun executeTest() {
        val list = emptyList<FavoriteEntity>()
        whenever(favoriteRepository.getAllFavorite()).thenReturn(Single.just(list))

        val observer = TestObserver<List<Movie>>()
        favoriteUseCase.execute().subscribe(observer)

        verify(favoriteRepository).getAllFavorite()
        observer.assertNoErrors()
        observer.assertComplete()
    }

    @Test
    fun addFavoriteTest() {
        whenever(favoriteRepository.addFavorite(favoriteEntity)).then { }

        favoriteUseCase.addFavorite(movie)

        //TODO add verify
    }

    @Test
    fun deleteFavorite() {
        whenever(favoriteRepository.deleteFavorite(favoriteEntity)).then { }

        favoriteUseCase.deleteFavorite(movie)

        //TODO add verify
    }
}