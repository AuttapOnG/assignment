package com.example.data.repository

import com.example.data.MovieService
import com.example.data.database.FavoriteEntity
import com.example.data.model.search.SearchMovieRequest
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.powermock.core.classloader.annotations.PrepareForTest
import org.powermock.modules.junit4.PowerMockRunner

@RunWith(PowerMockRunner::class)
@PrepareForTest(SearchMovieRequest::class)
class MovieRepositoryTest {
    lateinit var movieRepository: MovieRepository

    @Mock
    lateinit var movieService: MovieService

    @Mock
    lateinit var searchMovieRequest: SearchMovieRequest

    @Before
    fun setUp() {
        movieRepository = MovieRepositoryImpl(movieService)
    }

    @Test
    fun searchMovie() {
        whenever(movieService.searchMovie(searchMovieRequest.toParameters())).then(mock())

        movieRepository.searchMovie(searchMovieRequest)

        verify(movieService).searchMovie(searchMovieRequest.toParameters())
    }
}