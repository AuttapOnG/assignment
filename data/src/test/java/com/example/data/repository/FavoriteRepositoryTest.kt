package com.example.data.repository

import com.example.data.database.FavoriteDao
import com.example.data.database.FavoriteEntity
import com.nhaarman.mockitokotlin2.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.powermock.core.classloader.annotations.PrepareForTest
import org.powermock.modules.junit4.PowerMockRunner

@RunWith(PowerMockRunner::class)
@PrepareForTest(FavoriteEntity::class)
class FavoriteRepositoryTest {
    lateinit var favoriteRepository: FavoriteRepository

    @Mock
    lateinit var favoriteDao: FavoriteDao

    @Mock
    lateinit var favoriteEntity: FavoriteEntity

    @Before
    fun setUp() {
        favoriteRepository = FavoriteRepositoryImpl(favoriteDao)
    }

    @Test
    fun getAllFavoriteTest() {
        whenever(favoriteDao.getAllFavorite()).then(mock())

        favoriteRepository.getAllFavorite()

        verify(favoriteDao).getAllFavorite()
    }

    @Test
    fun addFavoriteTest() {
        whenever(favoriteDao.insertFavoriteEntity(favoriteEntity)).then(mock())

        favoriteRepository.addFavorite(favoriteEntity)

        verify(favoriteDao).insertFavoriteEntity(favoriteEntity)
    }

    @Test
    fun deleteFavoriteTest() {
        whenever(favoriteDao.deleteFavoriteEntity(favoriteEntity)).then(mock())

        favoriteRepository.deleteFavorite(favoriteEntity)

        verify(favoriteDao).deleteFavoriteEntity(favoriteEntity)
    }
}