package com.example.data.repository

import com.example.data.database.SearchHistoryDao
import com.example.data.database.SearchHistoryEntity
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.powermock.core.classloader.annotations.PrepareForTest
import org.powermock.modules.junit4.PowerMockRunner

@RunWith(PowerMockRunner::class)
@PrepareForTest(SearchHistoryEntity::class)
class SearchHistoryRepositoryTest {
    private lateinit var searchHistoryRepository: SearchHistoryRepository

    @Mock
    lateinit var searchHistoryDao: SearchHistoryDao

    @Mock
    lateinit var searchHistoryEntity: SearchHistoryEntity

    @Before
    fun setUp() {
        searchHistoryRepository = SearchHistoryRepositoryImpl(searchHistoryDao)
    }

    @Test
    fun insertTest() {
        whenever(searchHistoryDao.insertChatRoomEntity(searchHistoryEntity)).then(mock())

        searchHistoryRepository.insert(searchHistoryEntity)

        verify(searchHistoryDao).insertChatRoomEntity(searchHistoryEntity)
    }

    @Test
    fun getAllTest() {
        whenever(searchHistoryDao.getAllChatRoomEntity()).then(mock())

        searchHistoryRepository.getAll()

        verify(searchHistoryDao).getAllChatRoomEntity()
    }
}