package com.example.data.model.search

import com.google.gson.annotations.SerializedName

data class SearchResponse(
    @SerializedName("total_pages") val totalPages: Int,
    @SerializedName("page") val page: Int,
    @SerializedName("results") val results: List<MovieEntity>
)

data class MovieEntity(
    @SerializedName("id") val id: Int,
    @SerializedName("title") val title: String,
    @SerializedName("backdrop_path") val backdropPath: String,
    @SerializedName("overview") val overview: String,
    @SerializedName("release_date") val releaseDate: String,
    @SerializedName("vote_average") val voteAverage: Double
)