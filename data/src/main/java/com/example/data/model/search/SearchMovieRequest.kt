package com.example.data.model.search

import com.example.data.model.BaseRequest
import com.example.data.model.Parameters

data class SearchMovieRequest(private val title: String, private val paging: Int) : BaseRequest {
    override fun toParameters(): Parameters {
        return mapOf(
            "query" to title,
            "page" to paging
        )
    }
}