package com.example.data.model

interface BaseRequest {
    fun toParameters(): Parameters
}