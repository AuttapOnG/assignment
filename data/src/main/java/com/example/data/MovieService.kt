package com.example.data

import com.example.data.model.Parameters
import com.example.data.model.search.SearchResponse
import io.reactivex.Flowable
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface MovieService {
    @GET("api/movies/search")
    fun searchMovie(@QueryMap paramenter: Parameters): Single<SearchResponse>
}