package com.example.data.database

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "search_history_db")
class SearchHistoryEntity(@PrimaryKey var text: String)
