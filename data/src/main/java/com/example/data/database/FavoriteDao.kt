package com.example.data.database

import androidx.room.*
import io.reactivex.Single

@Dao
interface FavoriteDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertFavoriteEntity(entity: FavoriteEntity)

    @Query("SELECT * FROM favorite_db")
    fun getAllFavorite(): Single<List<FavoriteEntity>>

    @Delete
    fun deleteFavoriteEntity(favoriteEntity: FavoriteEntity)
}