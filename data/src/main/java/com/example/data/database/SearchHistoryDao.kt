package com.example.data.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Single

@Dao
interface SearchHistoryDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertChatRoomEntity(entity: SearchHistoryEntity)

    @Query("SELECT * FROM search_history_db")
    fun getAllChatRoomEntity(): Single<List<SearchHistoryEntity>>
}