package com.example.data.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "favorite_db")
class FavoriteEntity(
    @PrimaryKey var id: Int,
    @ColumnInfo(name = "title") var title: String,
    @ColumnInfo(name = "overview") var overview: String?,
    @ColumnInfo(name = "backdropPath") var backdropPath: String?,
    @ColumnInfo(name = "releaseDate") var releaseDate: String,
    @ColumnInfo(name = "voteAverage") var voteAverage: Double
)