package com.example.data.repository

import com.example.data.database.FavoriteDao
import com.example.data.database.FavoriteEntity
import io.reactivex.Single

interface FavoriteRepository {
    fun getAllFavorite(): Single<List<FavoriteEntity>>
    fun addFavorite(favoriteEntity: FavoriteEntity)
    fun deleteFavorite(favoriteEntity: FavoriteEntity)
}

class FavoriteRepositoryImpl constructor(private val favoriteDao: FavoriteDao) :
    FavoriteRepository {

    override fun getAllFavorite(): Single<List<FavoriteEntity>> {
        return favoriteDao.getAllFavorite()
    }

    override fun addFavorite(favoriteEntity: FavoriteEntity) {
        favoriteDao.insertFavoriteEntity(favoriteEntity)
    }

    override fun deleteFavorite(favoriteEntity: FavoriteEntity) {
        favoriteDao.deleteFavoriteEntity(favoriteEntity)
    }
}