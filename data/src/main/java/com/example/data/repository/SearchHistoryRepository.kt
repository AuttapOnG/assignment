package com.example.data.repository

import com.example.data.database.SearchHistoryDao
import com.example.data.database.SearchHistoryEntity
import io.reactivex.Single

interface SearchHistoryRepository {
    fun insert(searchHistoryEntity: SearchHistoryEntity)

    fun getAll(): Single<List<SearchHistoryEntity>>
}

class SearchHistoryRepositoryImpl constructor(private val searchHistoryDao: SearchHistoryDao) :
    SearchHistoryRepository {
    override fun insert(searchHistoryEntity: SearchHistoryEntity) {
        searchHistoryDao.insertChatRoomEntity(searchHistoryEntity)
    }

    override fun getAll(): Single<List<SearchHistoryEntity>> {
        return searchHistoryDao.getAllChatRoomEntity()
    }
}