package com.example.data.repository

import com.example.data.MovieService
import com.example.data.model.search.SearchMovieRequest
import com.example.data.model.search.SearchResponse
import io.reactivex.Single
import retrofit2.Retrofit

interface MovieRepository {
    fun searchMovie(searchMovieRequest: SearchMovieRequest): Single<SearchResponse>
}

class MovieRepositoryImpl constructor(private val movieService: MovieService) : MovieRepository {

    override fun searchMovie(searchMovieRequest: SearchMovieRequest): Single<SearchResponse> {
        return movieService.searchMovie(searchMovieRequest.toParameters())
    }
}